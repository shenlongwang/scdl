# Semi-Coupled Dictionary Learning

This repo is the folder for training and testing semi-coupled dictionary learning for super-resolution.
Author: Shenlong Wang, NPU/HK POLYU
Latest Update: 6, June 2012

## Citing
This code is for the paper:
S. Wang, L. Zhang, Y. Liang and Q. Pan, "Semi-coupled Dictionary Learning 
with Applications in Super-resolution and Photo-Sketch Synthesis", in CVPR 2012. 
It has been tested on 64-bit LINUX. 

## Acknowledgements
Thanks Dr. Weisheng Dong for providing some useful MATLAB functions including clustering and nonlocal block matching. 
The sparse coding algorithm used in this package is from SPAMS toolbox [2]. Please make sure that the toolbox has been 
successfully installed in your computer. 

- `collectPatches` : Training patches collecting
- `Dict_Train.m`   : Semi-coupled dictionary learning
- `Image_SR.m`     : Image interpolation demo 

## Pre-trained dictionary
Please download the pre-trained dictionaries (`SCDL_Data.zip`) from the project website and copy the MAT file to the Data subfolder if you want to evaluate the super-resolution/interpolation performance.

## References
Plese cite those paper below if you use this software:

[1] S. Wang, L. Zhang, Y. Liang and Q. Pan, "Semi-coupled Dictionary Learning 
with Applications in Super-resolution and Photo-Sketch Synthesis", in CVPR 2012. 

[2] J. Mairal, F. Bach, J. Ponce and G. Sapiro, "Online Dictionary
Learning for Sparse Coding", in ICML 2009

## Contact: 
- `{csslwang, cslzhang}@comp.polyu.edu.hk`